- In base.html:
  - pelican_dynamic plugin - styles
  - pelican_dynamic plugin - scripts
  - Check for BANNER_ALL_PAGES = False to not include bodypadding.js
  - Ignore bodypadding.js comopletely
- In archive.html:
  - Remove index.html in URLs by using MONTH_ARCHIVE_URL
  - Add class .icon-label to month texts
- In banner.html:
  - Move #banner to static/css/banner.css. You have to set its url manually.
- Remove bodypadding.js completely.

- Remove unnecessary min.css (keep yeti.min.css, keep silver pygments)

- In article.html
  - Add id="article-content" to div.entry-content
  
- Add ABSOLUTE_SITEURL. Use relative urls with SITEURL = '', only <link canonical and feeds use ABSOLUTE_SITEURL.
  
grep -rl 'SITEURL' . | xargs sed -i 's/SITEURL/__SITEURL/g'
grep -rl '__SITEURL' . | xargs sed -i 's/__SITEURL/SITEURL/g'

- Remove width: 100% from .table in bootstrap.yeti.min.css
- Change navbar colors in bootstrap.yeti.min.css

- In include/comment.html
  - Add CORAL_TALK