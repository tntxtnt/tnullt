#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Tri Tran'
SITENAME = 'tnullt'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Chicago'

DEFAULT_LANG = 'English'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = () #('Pelican', 'http://getpelican.com/'),

# Social widget
SOCIAL = () #('You can add links in your config file', '#'),

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Don't let Pelican process .html files
READERS = {"html": None}



###########################
## Pelican customization ##
###########################
USE_FOLDER_AS_CATEGORY = True

IGNORE_FILES = []

STATIC_PATHS = ['static', '404.html', '.well-known/acme-challenge']

# URL prettifiers
ARCHIVES_URL = 'archives/'
ARTICLE_URL = 'post/{slug}/'
AUTHOR_URL = 'author/{slug}/'
AUTHORS_URL = 'authors/'
CATEGORY_URL = 'category/{slug}/'
CATEGORIES_URL = 'categories/'
DRAFT_URL = 'draft/{slug}/'
MONTH_ARCHIVE_URL = 'archive/{date:%Y}/{date:%m}/'
PAGE_URL = 'page/{slug}/'
TAG_URL = 'tag/{slug}/'
TAGS_URL = 'tags/'

ARCHIVES_SAVE_AS = 'archives/index.html'
ARTICLE_SAVE_AS = 'post/{slug}/index.html'
AUTHOR_SAVE_AS = 'author/{slug}/index.html'
AUTHORS_SAVE_AS = 'authors/index.html'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
CATEGORIES_SAVE_AS = 'categories/index.html'
DRAFT_SAVE_AS = 'draft/{slug}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archive/{date:%Y}/{date:%m}/index.html'
PAGE_SAVE_AS = 'page/{slug}/index.html'
TAG_SAVE_AS = 'tag/{slug}/index.html'
TAGS_SAVE_AS = 'tags/index.html'

# Pelican's plugins
PLUGIN_PATHS = ['./plugins']
PLUGINS = ['pelican_dynamic', 'i18n_subsites', 'tag_cloud', 'bootstrapify',
    'section_number']

# Python-Markdown's extensions config
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5'
}

# Theme settings
THEME = './theme/pelican-bootstrap3'
BOOTSTRAP_THEME = 'yeti'
THEME_STATIC_DIR = 'static'
BOOTSTRAP_NAVBAR_INVERSE = True
PYGMENTS_STYLE = 'silver'
SITELOGO = 'static/img/TT.png'
SITELOGO_SIZE = 20
FAVICON = 'static/img/TT.png'
DISPLAY_TAGS_ON_SIDEBAR = True
USE_OPEN_GRAPH = False
DISPLAY_TAGS_INLINE = True
DISPLAY_ARCHIVE_ON_SIDEBAR = True
DISPLAY_CATEGORIES_ON_SIDEBAR = False
DISPLAY_RECENT_POSTS_ON_SIDEBAR = True
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}
BOOTSTRAPIFY = {
    'img': ['img-responsive', 'center-block']
}
# Banner
BANNER_CSS_FILE = 'static/css/banner.css'
BANNER_ALL_PAGES = False
BANNER_SUBTITLE = '... no caps!'

# Comments (experiment)
CORAL_TALK = True
