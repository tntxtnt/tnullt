document.getElementById("loadComments").onclick = function() {
  var loadBtn =  document.getElementById("loadComments");
  loadBtn.innerHTML = "Starting Heroku... (may take 30s)";
  loadBtn.disabled = true;
  var scr = document.createElement("script");
  scr.setAttribute("type", "text/javascript");
  scr.setAttribute("src", "https://tnullt-comment.herokuapp.com/embed.js");
  scr.onload = function() {
    Coral.Talk.render(document.getElementById('coral_talk'), {
      talk: 'https://tnullt-comment.herokuapp.com/'
    });
    loadBtn.className += " invisible";
  }
  document.getElementById("comments").appendChild(scr);
};