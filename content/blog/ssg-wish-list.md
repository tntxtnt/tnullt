---
Title: Static site generator wish list
Date: 2017-12-14
Tags: static site, hosting
Summary: New blog check list: responsive, no inline JS/CSS, code highlighting, math inline and display (KaTeX preferred), superscript, subscript, responsive images, nice table, ...
Scripts: katex-bundle.min.js
Styles: katex.min.css
---

## Wish list for a new blog:

- Responsive
- No inline JS/CSS
- Code highlighting
- Math: inline and display, KaTeX preferred
- Superscript, subscript
- Responsive images
- Beautiful tables
- Preserve TAB in code blocks
- URLs prettifying
- Comments

## Wish list for a new hosting:  

- Custom response headers (to add CSP, XSS Protection, etc.)
- Manual/auto renew certificates (4096-bit RSA)
- Force HTTPS
- Easy connecting to domain/subdomain (no validation bullshit like Google)
- ... not use CDN (because I have to allow *.cloudabc.xyz in my CSP)

## Check list

### Inline code
This is an inline code: `int n = 10`

### Code without line number

```text
    #include <iostream>
    int main() { std::cout << "Hello World!\n"; }
```
    
renders to

    #include <iostream>
    int main() { std::cout << "Hello World!\n"; }

### Code with line number

```text
    #!C++
    #include <iostream>
    int main() { std::cout << "Hello World!\n"; }
```
        
renders to

    #!C++
    #include <iostream>
    int main() { std::cout << "Hello World!\n"; }
    
Weird, `int` can be both `kr` (Keyword.Reserved) and `kt` (Keyword.Type). If you specify language with ` ```C++` it will parse correctly:

```C++
#include <iostream>
int main() { std::cout << "Hello World!\n"; }
```


[https://www.npmjs.com/package/pygments-tokens](https://www.npmjs.com/package/pygments-tokens)

### Inline math
Add

```text
Scripts: katex-bundle.min.js
Styles: katex.min.css
```

to post header.

Choose `\\(...\\)` as inline math bracket, `\\\\(...\\\\)` as display math bracket.
    
Inline math for `\\(e^{i\pi} = -1\\)` is \\(e^{i\pi} = -1\\)

### Display math

```
\\\\(
x = \begin{cases}
   a &\text{if } b  \\\\
   c &\text{if } d
\end{cases}
\\\\)
```
renders to
\\\\(
x = \begin{cases}
   a &\text{if } b  \\\\
   c &\text{if } d
\end{cases}
\\\\)

[https://khan.github.io/KaTeX/function-support.html](https://khan.github.io/KaTeX/function-support.html)

Remember: `\\` is `\`

### Superscript & Subscript
2^10^

H~2~O

Fine. I'll use `<sup>` and `<sub>`. 2<sup>10</sup>, H<sub>2</sub>O

### 7. Responsive images
![tnullt's logo](/static/img/TT.png)
![banner](/static/img/1024x256-min-tl-tri.png)

### Table

| Rank | Name   | Score   |
|:----:| ------ | -------:|
|   1  | Bob    | `29885` |
|   2  | Jon    | `17652` |
|   3  | Tyrion |  `9551` |

| m↓ p→  |         <center>1</center>        |         <center>3</center>        |        <center>5</center>         |         <center>7</center>        |
|:------:|:---------------------------------:|:---------------------------------:|:---------------------------------:|:---------------------------------:|
| **1**  | \\(C_a^\{1\} \times C_b^\{0\}\\)  |                                   |                                   |                                   |
| **2**  | \\(C_a^\{1\} \times C_b^\{1\}\\)  |                                   |                                   |                                   |
| **3**  | \\(C_a^\{1\} \times C_b^\{2\}\\)  | \\(C_a^\{3\} \times C_b^\{0\}\\)  |                                   |                                   |
| **4**  | \\(C_a^\{1\} \times C_b^\{3\}\\)  | \\(C_a^\{3\} \times C_b^\{1\}\\)  |                                   |                                   |
| **5**  | \\(C_a^\{1\} \times C_b^\{4\}\\)  | \\(C_a^\{3\} \times C_b^\{2\}\\)  | \\(C_a^\{5\} \times C_b^\{0\}\\)  |                                   |
| **6**  | \\(C_a^\{1\} \times C_b^\{5\}\\)  | \\(C_a^\{3\} \times C_b^\{3\}\\)  | \\(C_a^\{5\} \times C_b^\{1\}\\)  |                                   |
| **7**  | \\(C_a^\{1\} \times C_b^\{6\}\\)  | \\(C_a^\{3\} \times C_b^\{4\}\\)  | \\(C_a^\{5\} \times C_b^\{2\}\\)  | \\(C_a^\{7\} \times C_b^\{0\}\\)  |
|**Tổng**| \\(C_a^\{1\} \times \sum\limits_\{i=0\}^\{b\}\{C_b^\{i\}\}\\)  | \\(C_a^\{3\} \times \sum\limits_\{i=0\}^\{b\}\{C_b^\{i\}\}\\)  | \\(C_a^\{5\} \times \sum\limits_\{i=0\}^\{b\}\{C_b^\{i\}\}\\)  | \\(C_a^\{7\} \times \sum\limits_\{i=0\}^\{b\}\{C_b^\{i\}\}\\) |

Holy shit, I have to escape `{}` inside table with `\{\}` wtf is happenning??? Maybe because I don't have a fucking plugin like render_math for KaTeX in Pelican.

### Tab in code blocks
```
all:
	$(CXX) main.cpp
```
The technology just isn't there yet for Python-Markdown. Hugo solves this, but Hugo is full of bugs!

### Hosting
netlify.toml
```
[[headers]]
for = "/*"
[headers.values]
Content-Security-Policy = "default-src 'none'; script-src 'self'; style-src 'self'; img-src 'self'; object-src 'none'; frame-ancestors 'self'; font-src 'self'"
X-Content-Type-Options = "nosniff"
X-Frame-Options = "DENY"
X-XSS-Protection = "1; mode=block"
Referrer-Policy = "no-referrer"

[[headers]]
for = "/*.@(jpg|jpeg|gif|png|css|js)"
[headers.values]
Cache-Control = "max-age=604800"
```
Check score on [https://observatory.mozilla.org/](https://observatory.mozilla.org/): 120/100.

By adding Coral Talk for comments, we have to change `default-src` from none to our Talk app. 115/100 Observatory score.
```
[[headers]]
for = "/*"
[headers.values]
Content-Security-Policy = "default-src 'self' https://tnullt-comment.herokuapp.com; script-src 'self' https://tnullt-comment.herokuapp.com/embed.js; style-src 'self'; img-src 'self'; object-src 'none'; frame-ancestors 'self'; font-src 'self'"
X-Content-Type-Options = "nosniff"
X-Frame-Options = "DENY"
X-XSS-Protection = "1; mode=block"
Referrer-Policy = "no-referrer"

[[headers]]
for = "/*.@(jpg|jpeg|gif|png|css|js)"
[headers.values]
Cache-Control = "max-age=604800"
```

More checks: embed videos, sounds, ...?